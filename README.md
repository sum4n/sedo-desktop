# SEDO-BSPWM

## All of the color scheme looks may have changed and images are also may not be updated so Please take it with some edge or look at yourself

## GruvBox-Material
![gruv](https://user-images.githubusercontent.com/90515965/232206556-01898ffc-56e3-4ab9-8ed2-47d81a0ec7a7.png)

## EverForst
![ever_forest](https://user-images.githubusercontent.com/90515965/232206548-f5122570-8282-4770-a3cc-a8cb28b2544e.png)

## Gotham
![gotham](https://user-images.githubusercontent.com/90515965/232206552-bcc3c402-c37b-46e5-a4ba-648b6caa9736.png)

## Dracula
![dracula](https://user-images.githubusercontent.com/90515965/232206541-3c329257-6dc4-4b42-ae84-12b4dbf689ec.png)

## Doom-One
![doom_one](https://user-images.githubusercontent.com/90515965/232206442-ff9240d9-76de-4b4b-a6e1-8f1ba9e522c0.png)

## TokyoNight
![tokyo_night](https://user-images.githubusercontent.com/90515965/232206620-6dff8967-dff8-44e1-9e06-af946c6da330.png)

### Rofi
![rofi-menu](https://user-images.githubusercontent.com/90515965/232206711-1be6f19a-642c-4e8e-85de-738f65df78b7.png)

### PowerMenu
![power_menu](https://user-images.githubusercontent.com/90515965/232206727-c22f2d9e-cf03-4716-8506-c32ba1da653c.png)

### Themes Selector
![themes](https://user-images.githubusercontent.com/90515965/232206723-566a1ab6-87f5-4d96-832e-6cd561e3fc3c.png)

### Rofi-centered
![rofi-center](https://user-images.githubusercontent.com/90515965/232206732-6a68de41-817f-4093-8da0-72cf60ce109b.png)

### 💠 Installation:

This installer can download and setup BSPWM window manager in vanilla **[DEBIAN-Testing]** or **[ARCH]** based machines.
No need any desktop environment for setup. It downloads all it's needed packages and setup.

## Install the rice via installer
- **First download the installer**
```sh
wget https://gitlab.com/sum4n/sedo-desktop/-/raw/master/sedo_installer
```
- **Now give it execute permissions**
```sh
chmod +x sedo_installer
```
- **Finally run the installer**
```sh
./sedo_installer
```

## OR Manually can follow these steps

### DEBIAN dependencies
```
sudo apt install mpv zsh hwinfo htop feh picom lightdm brightnessctl xautolock cron light bspwm \
file-roller mlocate pulsemixer rofi suckless-tools network-manager-gnome dunst policykit-1-gnome \
sxhkd nemo pcmanfm lxappearance qt5ct udiskie copyq fish nitrogen vim viewnior volumeicon-alsa \
ranger python3-pip npm mpd ncmpcpp mpc gparted pavucontrol kdeconnect ufw xclip maim git curl \
wget ripgrep polybar htop neofetch playerctl pcmanfm acpi gpick qt5-style-kvantum numlockx \
xfce4-settings i3lock-color xfce4-power-manager slop network-manager xorg xinit pulseaudio build-essential \
bluez blueman xdotool xdo autoconf exa lightdm-gtk-greeter lightdm-gtk-greeter-settings firefox-esr lf \
fd-find fzf alacritty cava socat jq kitty
```
### ARCH dependencies
```
sudo pacman -Sy lightdm fzf fd feh zsh htop hwinfo mpv kvantum numlockx brightnessctl light \
xautolock file-roller mlocate rofi-emoji pulsemixer bspwm rofi dmenu network-manager-applet \
dunst polkit-gnome sxhkd alacritty nemo lxappearance qt5ct udiskie copyq fish nitrogen vim \
neovim viewnior volumeicon pacman-contrib ranger python-pip npm mpd ncmpcpp mpc gparted yarn \
pavucontrol kdeconnect ufw gufw xclip maim git curl wget lazygit python-pywal ripgrep polybar \
ntfs-3g playerctl btop neofetch android-tools android-file-transfer pcmanfm acpi cronie libxcrypt-compat \
gpick fortune-mod xfce4-settings xfce4-power-manager networkmanager archlinux-keyring xorg \
xorg-xinit pulseaudio pulseaudio-alsa xf86-video-amdgpu base-devel bluez bluez-utils blueman \
xdotool xdo autoconf slop exa firefox lightdm-gtk-greeter lightdm-gtk-greeter-settings \
papirus-icon-theme lf socat jq kitty
```
* Clone the actual repo
```
git clone https://gitlab.com/sum4n/sedo-wm
```
* **Clone the scripts repo it's like a dependenci for work properly**
```
git clone https://gitlab.com/sum4n/bin
```
* clone the fonts repo it's (optional) if you want you can change your font
```
git clone https://gitlab.com/sum4n/sedo-fonts
```

## Lastly copy all the folders to their respected directory

* **Suman Kumar Das**
* [GitLab](https://gitlab.com/sum4n) 
* [GitHub](https://github.com/tosuman) 
