#!/usr/bin/env bash

# Author        - sum4n
# Source        - https://gitlab.com/sum4n/SEDO
# Maintainer    - to_suman@outlook.com

#---- launch function ----//
function launch() {
	local running
	running="$(pgrep -x "$1")"
	[ "$running" ] && kill -9 $(pgrep -x "$1") 2>/dev/null
	eval "$* &"
}

## -- global variables
[ -d "$HOME/.config/bspwm/scripts" ] && export PATH="${PATH}:$HOME/.config/bspwm/scripts"
[ -d "$HOME/SEDO/scripts" ] && export PATH="${PATH}:$HOME/SEDO/scripts"
[ -d "$HOME/.local/bin" ] && export PATH="${PATH}:$HOME/.local/bin"

#---- Launch startup programs ----//
launch sxhkd -c ~/.config/sxhkd/sxhkdrc
if ! pidof mpd >/dev/null; then
	mpd &
fi
launch xfce4-power-manager
launch numlockx on
launch blueman-applet

while pgrep -u $UID -x picom >/dev/null; do pkill picom; done
picom --config "$HOME"/.config/picom/picom.conf &

if command -v apt &>/dev/null; then
	launch /usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1
else
	launch /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
fi

launch nitrogen --restore
launch copyq
launch /usr/lib/kdeconnectd
launch kdeconnect-indicator
launch udiskie -a -n -t
while pgrep -u $UID -x naughty >/dev/null; do pkill naughty; done
launch dunst

#---- Start custom scripts ----//
launch locker
xsetroot -cursor_name left_ptr
