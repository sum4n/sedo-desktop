# Author     - sum4n
# Source     - https://gitlab.com/sum4n
# Maintainer - to_suman@outlook.com

if status is-interactive

# prompt
if command -v starship >/dev/null; eval (starship init fish); end

# disable welcome greet
set fish_greeting

### CUSTOM ALIASES ###
#-- ls and ll aliases using exa
alias ls 'exa -aG --color=always --icons --group-directories-first'
alias ll 'exa -Glah --icons --group-directories-first'

#-- package manager
if command -v pacman > /dev/null
    alias install 'sudo pacman -S'
    alias update 'sudo pacman -Syyu'
    alias sync 'sudo pacman -Syy'
    alias cleanup 'sudo pacman -Rns (pacman -Qtdq)'
    alias remove 'sudo pacman -Rns'
    alias search 'pacman -Ss'
else if command -v apt > /dev/null
    if command -v nala > /dev/null
        alias install 'sudo nala install'
        alias update 'sudo nala update; sudo nala upgrade'
        alias sync 'sudo nala update'
        alias cleanup 'sudo nala autoremove'
        alias purge 'sudo nala autopurge'
        alias search 'nala search'
        alias showin 'nala show'
    else
        alias install 'sudo apt install'
        alias update 'sudo apt-get update; sudo apt-get upgrade'
        alias sync 'sudo apt-get update'
        alias cleanup 'sudo apt autoremove'
        alias purge 'sudo apt autopurge'
        alias search 'apt search'
        alias showin 'apt show'
    end
else if command -v zypper > /dev/null
    alias install 'sudo zypper in'
    alias update 'sudo zypper up'
    alias dupdate 'sudo zypper dup'
    alias cleanup 'sudo zypper remove -u'
    alias search 'zypper se'
end

#-- systemd
alias listctl 'systemctl list-unit-files --state=enabled'

#-- get top processes consuming memory and CPU
alias load-mem 'ps auxf | sort -nr -k 4 | head -5'
alias load-cpu 'ps auxf | sort -nr -k 3 | head -5'

#-- Fix for when keys break
alias archlinx-fix-keys 'sudo pacman-key --init; sudo pacman-key --populate archlinux; sudo pacman-key --refresh-keys'

#-- Directory navigation aliases
alias cdsed 'cd $HOME/sedo-desktop'
alias cdcfg 'cd ~/.config/'
alias cdnv 'cd ~/.config/nvim/'
alias cdpol 'cd ~/.config/polybar/'
alias cdbsp 'cd ~/.config/bspwm/'

#-- makefile
alias mkf 'touch'

#-- easy directories navigation using fzf and find
function cdd
    set -l directory (find / -type d 2>/dev/null | fzf -i --reverse --ansi --preview-window=down --preview='exa -Glah --icons --group-directories-first {}')
    if test -n "$directory"
        cd "$directory" || echo "Error: Failed to change directory"
    end
end

#-- git
alias lgt 'lazygit'
alias gcl 'git clone'
alias ginit 'git init'
alias gad 'git add'
alias gcm 'git commit -m'
alias gp 'git push'
alias gst 'git status'

#-- git bare
alias bare '/usr/bin/git --git-dir=$HOME/Git/sedo-de --work-tree=$HOME'
alias bst 'bare status'
alias bad 'bare add'
alias bcm 'bare commit -m'
alias binit 'git init --bare'
alias utrack 'bare config --local status.showUntrackedFiles no'

#-- xampp
alias htdoc 'cd /opt/lampp/htdocs/'
alias xampp 'sudo /opt/lampp/xampp start'
alias xstop 'sudo /opt/lampp/xampp stop'

#-- Typo
alias cd.. 'cd ..'
alias pdw 'pwd'
alias clr 'clear'

#-- file permission
alias modx 'chmod +x'

#-- grep
alias grep 'grep --color=auto'
alias egrep 'egrep --color=auto'
alias fgrep 'fgrep --color=auto'

#-- readable output
alias df 'df -h'

#-- editors
alias nvm 'nvim'
alias vm 'vim'
alias sub 'subl'
alias nv 'nvim'

#-- free
alias free "free -mt"

#-- cat to bat
alias cat 'bat'

#-- continue download
alias wget "wget -c"

#-- userlist
alias usrlist "cut -d: -f1 /etc/passwd | sort"

#-- merge new settings
alias merge "xrdb -merge ~/.Xresources"

#-- switch between bash and zsh
alias tobash "sudo chsh $USER -s /bin/bash; echo 'Now log out.'"
alias tozsh "sudo chsh $USER -s /bin/zsh; echo 'Now log out.'"
alias tofish "sudo chsh $USER -s /bin/fish; echo 'Now log out.'"

#-- switch between lightdm and sddm
alias tolightdm "sudo pacman -S lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings --noconfirm --needed ; sudo systemctl enable lightdm.service -f ; echo 'Lightm is active - reboot now'"
alias tosddm "sudo pacman -S sddm --noconfirm --needed ; sudo systemctl enable sddm.service -f ; echo 'Sddm is active - reboot now'"
alias toly "sudo pacman -S ly --noconfirm --needed ; sudo systemctl enable ly.service -f ; echo 'Ly is active - reboot now'"

#-- hardware info --short
alias hw "hwinfo --short"

#-- youtube download
alias yta-aac "yt-dlp --extract-audio --audio-format aac "
alias yta-best "yt-dlp --extract-audio --audio-format best "
alias yta-flac "yt-dlp --extract-audio --audio-format flac "
alias yta-mp3 "yt-dlp --extract-audio --audio-format mp3 "
alias ytv-best "yt-dlp -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --merge-output-format mp4 "

#-- search content with ripgrep
alias rg "rg --sort path"

#-- get the error messages from journalctl
alias jctl "journalctl -p 3 -xb"

#-- power options
alias reboot "systemctl reboot"
alias shutd "systemctl poweroff"

#-- global files editor
function edit
    set -l file (find / -type f -print 2>/dev/null | fzf -i --ansi --reverse)
    if test -n "$file"
        if test (stat -c "%U" -- "$file") = "root"
            sudoedit "$file"
        else
            nvim "$file"
        end
    end
end

#-- give the list of all installed desktops - xsessions desktops
alias xdesk "ls /usr/share/xsessions"

# ex = EXtractor for all kinds of archives
# usage: ex <file>
function ex
    if test -f $argv[1]
        switch $argv[1]
            case '*.tar.bz2'
                tar xjf $argv[1]
            case '*.tar.gz'
                tar xzf $argv[1]
            case '*.bz2'
                bunzip2 $argv[1]
            case '*.rar'
                unrar x $argv[1]
            case '*.gz'
                gunzip $argv[1]
            case '*.tar'
                tar xf $argv[1]
            case '*.tbz2'
                tar xjf $argv[1]
            case '*.tgz'
                tar xzf $argv[1]
            case '*.zip'
                unzip $argv[1]
            case '*.Z'
                uncompress $argv[1]
            case '*.7z'
                7z x $argv[1]
            case '*.deb'
                ar x $argv[1]
            case '*.tar.xz'
                tar xf $argv[1]
            case '*.tar.zst'
                tar xf $argv[1]
            case '*'
                echo "'$argv[1]' cannot be extracted via ex()" >&2
        end
    else
        echo "'$argv[1]' is not a valid file" >&2
    end
end

end
