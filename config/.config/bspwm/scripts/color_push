#!/usr/bin/env bash
## A script to get colours from the theme file & deploy it to alacritty

color_palette="$(<"$HOME"/.config/bspwm/style.cfg)"
bspdir="$HOME/.config/bspwm"
config="$HOME/.config"
color="$bspdir/styles/$color_palette"

# Get colors
# shellcheck source=/dev/null
source "$color"/*.txt

## Get colors
getcolors() {
	FOREGROUND="$foreground"
	BACKGROUND="$background"
	CURSOR="$cursor"
	BLACK="$color0"
	RED="$color1"
	GREEN="$color2"
	YELLOW="$color3"
	BLUE="$color4"
	MAGENTA="$color5"
	CYAN="$color6"
	WHITE="$color7"
	ALTBLACK="$color8"
	ALTRED="$color9"
	ALTGREEN="$color10"
	ALTYELLOW="$color11"
	ALTBLUE="$color12"
	ALTMAGENTA="$color13"
	ALTCYAN="$color14"
	ALTWHITE="$color15"
}

## Write alacritty colors.yml file with current theme colors
alacritty() {
	cat >~/.config/alacritty/colors.yml <<-_EOF_
		## Colors configuration
		colors:
		  # Default colors
		  primary:
		    background: '${BACKGROUND}'
		    foreground: '${FOREGROUND}'
		    cursor: '${CURSOR}'

		  # Normal colors
		  normal:
		    black:   '${ALTBLACK}'
		    red:     '${RED}'
		    green:   '${GREEN}'
		    yellow:  '${YELLOW}'
		    blue:    '${BLUE}'
		    magenta: '${MAGENTA}'
		    cyan:    '${CYAN}'
		    white:   '${WHITE}'

		  # Bright colors
		  bright:
		    black:   '${ALTBLACK}'
		    red:     '${ALTRED}'
		    green:   '${ALTGREEN}'
		    yellow:  '${ALTYELLOW}'
		    blue:    '${ALTBLUE}'
		    magenta: '${ALTMAGENTA}'
		    cyan:    '${ALTCYAN}'
		    white:   '${ALTWHITE}'
	_EOF_
}

## Push colors for kitty terminal
kitty() {
	cat >"$config"/kitty/colors.ini <<-EOF
		    # colors for kitty -----//

		    background $BACKGROUND
		    foreground $FOREGROUND
        url_color $GREEN
		    
		    selection_background $FOREGROUND
		    selection_foreground $BACKGROUND
		    
		    cursor $CURSOR
		    cursor_text_color $BLACK
		    
		    # Black
		    color0 $ALTBLACK
		    color8 $ALTBLACK
		    
		    # Red
		    color1 $RED
		    color9 $ALTRED
		    
		    # Green
		    color2  $GREEN
		    color10 $ALTGREEN
		    
		    # Yellow
		    color3  $YELLOW
		    color11 $ALTYELLOW
		    
		    # Blue
		    color4  $BLUE
		    color12 $ALTBLUE
		    
		    # Magenta
		    color5  $MAGENTA
		    color13 $ALTMAGENTA
		    
		    # Cyan
		    color6  $CYAN
		    color14 $ALTCYAN
		    
		    # White
		    color7  $WHITE
		    color15 $ALTWHITE

	EOF

}

## Write rofi colors.rasi file with current theme colors
rofi() {
	cat >"$config"/rofi/themes/colors.rasi <<-EOF
		* {
		    background:         ${BACKGROUND};
		    background-alt:     ${ALTBLACK};
		    foreground:         ${FOREGROUND};
		    selected:           ${BLUE};
		    active:             ${MAGENTA};
		    highlight:          ${YELLOW};
		    urgent:             ${RED};
		    on:                 ${GREEN};
		    off:                ${RED};
		    BG:    ${BACKGROUND};
		    BGA:   ${ALTBLACK};
		    FG:    ${FOREGROUND};
		    BDR:   ${ALTBLACK};
		    SEL:   ${BLUE};
		    UGT:   ${RED};
		    IMG:   ${YELLOW};
		    ON:    ${GREEN};
		    OFF:   ${RED};
		}
	EOF
}

## Execute functions and run main
main() {
	getcolors
	alacritty
	rofi
	kitty
}

main
