#!/bin/env bash
#
# ~/.bashrc


# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

# source files
# shellcheck source=/dev/null
[ -d /usr/share/bash-completion/completions ] && source /usr/share/bash-completion/completions/*

source "$HOME/.config/zsh/alias"
source "$HOME/.config/zsh/env_value"

eval "$(starship init bash)"

