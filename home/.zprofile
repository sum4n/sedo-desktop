#!/bin/env bash
# this file is not read by bash(1), if ~/.bash_profile or ~/.bash_login exists.

### ( if running bash )
if [ -n "$BASH_VERSION" ]; then
    if [ -f "$HOME/.bashrc" ]; then
      # shellcheck source=/dev/null
    	source "$HOME/.bashrc"
    fi
fi

### set PATH so it includes user's private bin if it exists
[ -d "$HOME/bin" ] && PATH="$HOME/bin:$PATH"

### set PATH so it includes user's private bin if it exists
[ -d "$HOME/scripts" ] && PATH="$HOME/scripts:$PATH"

### set ~/.local/bin folder as a PATH variable
[ -d "$HOME/.local/bin" ] && export PATH="$HOME/.local/bin:$PATH"

### set PATH so it includes user's private bin if it exists
[ -d "$HOME/Applications" ] && PATH="$HOME/Applications:$PATH"

### XDG Paths
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_STATE_HOME=$HOME/.local/state
export XDG_DATA_HOME=$HOME/.local/share

### zsh config dir
export ZDOTDIR=$HOME/.config/zsh

### for cargo and rustup
[ -e "$HOME/.cargo/env" ] && source "$HOME/.cargo/env"

